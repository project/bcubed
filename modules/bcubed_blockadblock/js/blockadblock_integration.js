new BCubedEventGeneratorPlugin({
  init: function(args){
    var that = this;
    try {
      blockAdBlock = new BlockAdBlock({
        resetOnEnd: args.settings.resetOnEnd,
        loopCheckTime: args.settings.loopCheckTime,
        loopMaxNumber: args.settings.loopMaxNumber,
        baitClass: args.settings.baitClass,
        baitStyle: args.settings.baitStyle,
        debug: args.settings.debug,
        checkOnLoad: false
      });
      blockAdBlock.onDetected(function () {that.sendEvent('advancedAdblockerDetection', { detected: true });});
      blockAdBlock.onNotDetected(function () {
        if (navigator.userAgent.indexOf('Focus') !== -1 && navigator.platform.indexOf('iP') !== -1) {
          that.sendEvent('advancedAdblockerDetection', { detected: true });
        }
        else {
          if (args.settings.use_adscript){
            script = document.createElement('script');
            script.type = 'text/javascript';
            script.onerror = function() { that.sendEvent('advancedAdblockerDetection', { detected: true }); };
            script.async = true;
            script.onload = function() { that.sendEvent('advancedAdblockerDetection', { detected: false }); };
            script.src = args.settings.adscript;
            document.head.appendChild(script);
          }
          else {
            that.sendEvent('advancedAdblockerDetection', { detected: false });
          }
        }
      });
      blockAdBlock.check();
    }
    catch(e) {
      that.sendEvent('advancedAdblockerDetection', { detected: true });
    }
  }
});
