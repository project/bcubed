new BCubedConditionPlugin({
  condition: function(args) {
    for(var i = args.events.length -1; i >= 0 ; i--){
      if (args.events[i].type == "advancedAdblockerDetection"){
        return !args.events[i].detail.detected;
      }
    }
    return true;
  }
});
