<?php

namespace Drupal\bcubed_blockadblock\Plugin\bcubed\Condition;

use Drupal\bcubed\ConditionBase;

/**
 * Provides condition to restrict condition set.
 *
 * Restrict condition set to running on pages where
 * and adblocker has not been detected.
 *
 * @Condition(
 *   id = "advanced_adblocker_not_detected",
 *   label = @Translation("Adblocker Not Detected"),
 *   description = @Translation("Run only on pages where an adblocker has not been detected."),
 *   bcubed_dependencies = {
 *    {
 *      "plugin_type" = "event",
 *      "plugin_id" = "advanced_adblocker_detection",
 *      "same_set" = true,
 *      "dependency_type" = "requires",
 *    }
 *  }
 * )
 */
class AdblockerNotDetected extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function getJSCondition() {
    return 'babAdsNotBlocked';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibrary() {
    return 'bcubed_blockadblock/adsnotblocked';
  }

}
