<?php

namespace Drupal\bcubed_blockadblock\Plugin\bcubed\Condition;

use Drupal\bcubed\ConditionBase;

/**
 * Provides condition to restrict condition set.
 *
 * Restrict condition set to running on pages where
 * and adblocker has been detected.
 *
 * @Condition(
 *   id = "advanced_adblocker_detected",
 *   label = @Translation("Adblocker Detected"),
 *   description = @Translation("Run only on pages where an adblocker has been detected."),
 *   bcubed_dependencies = {
 *    {
 *      "plugin_type" = "event",
 *      "plugin_id" = "advanced_adblocker_detection",
 *      "same_set" = true,
 *      "dependency_type" = "requires",
 *    }
 *  }
 * )
 */
class AdblockerDetected extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function getJSCondition() {
    return 'babAdsBlocked';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibrary() {
    return 'bcubed_blockadblock/adsblocked';
  }

}
