<?php

namespace Drupal\bcubed_blockadblock\Plugin\bcubed\Event;

use Drupal\bcubed\EventBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides advanced adblocker detection event.
 *
 * @Event(
 *   id = "advanced_adblocker_detection",
 *   label = @Translation("Adblocker Detection (Advanced)"),
 *   description = @Translation("Advanced adblocker detection powered by BlockAdBlock"),
 *   settings = {
 *     "resetOnEnd" = 1,
 *     "loopCheckTime" = 50,
 *     "loopMaxNumber" = 5,
 *     "baitClass" = "pub_300x250 pub_300x250m pub_728x90 text-ad textAd text_ad text_ads text-ads text-ad-links",
 *     "baitStyle" = "width: 1px !important; height: 1px !important; position: absolute !important; left: -10000px !important; top: -1000px !important;",
 *     "debug" = 0,
 *     "use_adscript" = 0,
 *     "adscript" = "//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
 *   }
 * )
 */
class AdvancedAdblockerDetection extends EventBase {

  /**
   * {@inheritdoc}
   */
  public function getLibrary() {
    return 'bcubed_blockadblock/blockadblock_integration';
  }

  /**
   * {@inheritdoc}
   */
  public function getEventName() {
    return 'advancedAdblockerDetection';
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['resetOnEnd'] = [
      '#type' => 'checkbox',
      '#title' => 'Remove all events added at end of check',
      '#default_value' => $this->settings['resetOnEnd'],
    ];

    $form['loopCheckTime'] = [
      '#type' => 'number',
      '#title' => 'The number of milliseconds between each check',
      '#default_value' => $this->settings['loopCheckTime'],
      '#required' => TRUE,
    ];

    $form['loopMaxNumber'] = [
      '#type' => 'number',
      '#title' => 'Number of checks to perform',
      '#description' => $this->t('The number of negative checks after which there is considered that AdBlock is not enabled'),
      '#default_value' => $this->settings['loopMaxNumber'],
      '#required' => TRUE,
    ];

    $form['baitClass'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => 'CSS class used by the bait',
      '#default_value' => $this->settings['baitClass'],
      '#required' => TRUE,
    ];

    $form['baitStyle'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => 'CSS style used to hide the bait from display',
      '#default_value' => $this->settings['baitStyle'],
      '#required' => TRUE,
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => 'Display debugging info in the console',
      '#default_value' => $this->settings['debug'],
    ];

    $form['use_adscript'] = [
      '#type' => 'checkbox',
      '#title' => 'Use Ad Script for Detection',
      '#description' => $this->t('If the site uses a script to load ads, this script can be loaded from the browser cache to determine if an adblocker is present.'),
      '#default_value' => $this->settings['use_adscript'],
    ];

    $form['adscript'] = [
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#title' => 'URL of Ad Script to Check',
      '#description' => $this->t('Enter the exact same URL as used on the site for loading ads, to ensure that the browser cache is utilized.'),
      '#default_value' => $this->settings['adscript'],
      '#states' => [
        'visible' => [
          ':input[name="use_adscript"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="use_adscript"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

}
