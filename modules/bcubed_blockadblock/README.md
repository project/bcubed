Introduction
------------

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/bcubed_blockadblock>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/bcubed_blockadblock>


Requirements
------------

Depends on BCubed module:
<https://drupal.org/project/bcubed>

Requires BlockAdBlock library:
<https://github.com/sitexw/BlockAdBlock/>

BlockAdBlock must be installed in the libraries directory, so that blockadblock.js is available at:

    /libraries/blockadblock/blockadblock.js


Installation
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   <https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules>
   for further information.


Configuration
-------------
   
 * Manage Condition Sets in 
   Administration » Configuration » System » BCubed » Condition Sets.
   
 * Create a BCubed condition set and select the "Adblocker Detection (Advanced)" event.
 
 * Add one of the newly available conditions, "Adblocker Detected" or "Adblocker Not Detected".
 
 * Add any desired actions, and save the condition set.
 
 
 Notes
 -----
 
 If multiple condition sets configured with the "Adblocker Detection (Advanced)" event are active on a single page,
 only a single instance of BlockAdBlock will be loaded. This instance will be configured according to the settings
 of the condition set with the highest priority (occuring first in the listing of enabled condition sets).
