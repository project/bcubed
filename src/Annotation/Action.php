<?php

namespace Drupal\bcubed\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Action plugin item annotation object.
 *
 * @see \Drupal\bcubed\Plugin\ActionManager
 * @see plugin_api
 *
 * @Annotation
 */
class Action extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Additional administrative information about the action's behavior.
   *
   * @var \Drupal\Core\Annotation\Translationoptional
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * The default settings for the action.
   *
   * @var arrayoptional
   */
  public $settings = [];

  /**
   * Whether action can be instantiated (run more than one instance)
   *
   * @var booloptional
   */
  public $instances = FALSE;

  /**
   * The BCubed plugin dependencies of this plugin.
   *
   * Example dependencies array, in annotation form:
   * bcubed_dependencies = {
   *  {
   *    "plugin_type" = "event",
   *    "plugin_id" = "some_event",
   *    "same_set" = true,
   *    "dependency_type" = "requires",
   *  }
   * }
   *
   * @var arrayoptional
   */
  public $bcubed_dependencies = [];

  /**
   * Any generated strings registered by the plugin.
   *
   * @var arrayoptional
   */
  public $generated_strings = [];

  /**
   * Key to use for generated strings. Defaults to plugin id.
   *
   * @var stringoptional
   */
  public $generated_strings_dictionary = '';

}
